/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {Intro} from './src/ui/pages/tugas1/Intro';
import {Biodata} from './src/ui/pages/tugas2/Biodata';
import {Todo} from './src/ui/pages/tugas3/TodoList';
import {Context} from './src/ui/pages/tugas4';
import {Navigation} from './src/ui/pages/Tugas11/Navigation';
// import AppNavigation from './src/ui/pages/tugas5/navigation';
import AppNavigation from './src/ui/pages/Tugas6';
import firebase from '@react-native-firebase/app';

const firebaseConfig = {
  apiKey: 'AIzaSyDfRY40V1IF38pDtFmri4NUtxTguU8gpU0',
  authDomain: 'sanbercode-84d17.firebaseapp.com',
  databaseURL: 'https://sanbercode-84d17.firebaseio.com',
  projectId: 'sanbercode-84d17',
  storageBucket: 'sanbercode-84d17.appspot.com',
  messagingSenderId: '212518144410',
  appId: '1:212518144410:web:8013cb87988920fa985223',
  measurementId: 'G-DVMN5S9NQF',
};
// Inisialisasi firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App = () => {
  return <Navigation />;
};

export default App;
