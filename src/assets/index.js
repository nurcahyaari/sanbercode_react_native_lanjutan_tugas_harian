const logoIcon = require('./img/logo.jpg');
const moneyBagIcon = require('./img/money-bag.png');
const researchIcon = require('./img/research.png');
const ventureIcon = require('./img/venture.png');
const workingTimeIcon = require('./img/working-time.png');

export {logoIcon, moneyBagIcon, researchIcon, ventureIcon, workingTimeIcon};
