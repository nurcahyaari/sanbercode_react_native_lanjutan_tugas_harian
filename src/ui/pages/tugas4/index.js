import React, {useState, createContext, useCallback} from 'react';
import {Todo} from './TodoList';
import moment from 'moment';

export const RootContext = createContext();

export const Context = () => {
  const [input, setInput] = useState('');
  const [todos, setTodos] = useState([]);

  const handleChangeInput = (val) => setInput(val);
  const handleAddTodos = () => {
    setInput('');
    setTodos([
      ...todos,
      {
        id: moment().valueOf(),
        name: input,
        date: moment().format('D/M/YYY'),
      },
    ]);
  };

  const handleDeleteTodos = useCallback(
    (id) => (e) => {
      setTodos((todo) => todo.filter((item) => item.id !== id));
    },
    [],
  );
  return (
    <RootContext.Provider
      value={{
        todos,
        input,
        handleAddTodos,
        handleChangeInput,
        handleDeleteTodos,
      }}>
      <Todo />
    </RootContext.Provider>
  );
};
