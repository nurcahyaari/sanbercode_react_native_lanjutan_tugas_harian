import React, {useState, useCallback, useContext} from 'react';
import {FlatList, View, Text, TouchableOpacity, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import moment from 'moment';

import {RootContext} from './index';

const List = (props) => (
  <View
    key={props.id}
    style={{
      flex: 1,
      marginTop: 10,
      backgroundColor: 'grey',
      justifyContent: 'center',
    }}>
    <View
      style={{
        backgroundColor: 'white',
        margin: 4,
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <View style={{flexDirection: 'column'}}>
        <Text style={{margin: 4}}>{props.date}</Text>
        <Text style={{margin: 4}}>{props.name}</Text>
      </View>
      <View style={{justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={props.handleDeleteTodos}
          style={{
            flex: 1,
            marginLeft: 21,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon name="delete" size={30} color="#000" />
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

export const Todo = () => {
  const state = useContext(RootContext);

  return (
    <View style={{padding: 21, flex: 1}}>
      <View style={{height: 60, marginBottom: 10}}>
        <View>
          <Text>Masukan Todolist</Text>
        </View>
        <View style={{flexDirection: 'row', flex: 1}}>
          <TextInput
            style={{
              // height: 40,
              borderColor: 'gray',
              borderWidth: 1,
              width: '80%',
            }}
            onChangeText={state.handleChangeInput}
            value={state.input}
          />
          <View style={{width: '20%'}}>
            <TouchableOpacity
              onPress={state.handleAddTodos}
              style={{
                backgroundColor: '#58d4ed',
                flex: 1,
                marginLeft: 21,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name="plus" size={30} color="#000" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={{flex: 1}}>
        <FlatList
          data={state.todos}
          renderItem={({item}) => (
            <List
              {...item}
              handleDeleteTodos={state.handleDeleteTodos(item.id)}
            />
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </View>
  );
};
