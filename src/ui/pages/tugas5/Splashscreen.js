import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

import {logoIcon} from '../../../assets';

function SplashScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Image source={logoIcon} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  logoContainer: {
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
