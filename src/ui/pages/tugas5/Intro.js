import React from 'react';
import {View, Text, Image, StatusBar} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider'; //import library atau module react-native-app-intro-slider
import Icon from 'react-native-vector-icons/Ionicons';
import {
  workingTimeIcon,
  researchIcon,
  ventureIcon,
  moneyBagIcon,
} from '../../../assets';
//data yang akan digunakan dalam onboarding
const slides = [
  {
    key: 1,
    title: 'Belajar Intensif',
    text:
      '4 pekan online, 5 hari sepekan, estimasi materi dan tugas 3-4 jam per hari',
    image: workingTimeIcon,
  },
  {
    key: 2,
    title: 'Teknologi Populer',
    text: 'Menggunakan bahasa pemrograman populer',
    image: researchIcon,
  },
  {
    key: 3,
    title: 'From Zero to Hero',
    text: 'Tidak ada syarat minimum skill, cocok untuk pemula',
    image: ventureIcon,
  },
  {
    key: 4,
    title: 'Training Gratis',
    text: 'Kami membantu Anda mendapatkan pekerjaan / proyek',
    image: moneyBagIcon,
  },
];

const Intro = ({navigation}) => {
  //menampilkan data slides kedalam renderItem
  const renderItem = ({item}) => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontWeight: 'bold'}}>{item.title}</Text>
        <Image source={item.image} style={{width: 128, height: 128}} />
        <Text style={{fontSize: 16}}>{item.text}</Text>
      </View>
    );
  };

  //fungsi ketika onboarding ada di list terakhir atau screen terakhir / ketika button done di klik
  const onDone = () => {
    navigation.navigate('Login');
  };

  //mengcustom tampilan button done
  const renderDoneButton = () => {
    return (
      <View
        style={{
          flex: 1,
          borderRadius: 100,
          width: 50,
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'blue',
        }}>
        <Icon name="md-checkmark" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  //mengcustom tampilan next button
  const renderNextButton = () => {
    return (
      <View
        style={{
          backgroundColor: 'blue',
          justifyContent: 'center',
          alignItems: 'center',
          width: 50,
          height: 50,
          borderRadius: 100,
        }}>
        <Icon name="arrow-forward" color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  return (
    <View style={{flex: 1, justifyContent: 'center'}}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <View style={{flex: 1}}>
        {/* merender atau menjalankan library react-native-app-intro-slider */}
        <AppIntroSlider
          data={slides}
          onDone={onDone}
          renderItem={renderItem}
          renderDoneButton={renderDoneButton}
          renderNextButton={renderNextButton}
          keyExtractor={(item, index) => index.toString()}
          activeDotStyle={{backgroundColor: '#191970'}}
        />
      </View>
    </View>
  );
};

export default Intro;
