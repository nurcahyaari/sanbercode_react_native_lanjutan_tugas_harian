import React from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {logoIcon} from '../../../assets';
function Login({navigation}) {
  return (
    <ScrollView style={{padding: 21, backgroundColor: '#fff'}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={logoIcon} style={{flex: 1}} />
      </View>
      <View style={{flex: 1}}>
        <View style={{marginBottom: 10}}>
          <Text>Username</Text>
          <TextInput
            style={{borderBottomColor: '#000', borderBottomWidth: 1}}
            placeholder="Username/ Password"
          />
        </View>
        <View style={{marginBottom: 10}}>
          <Text>Password</Text>
          <TextInput
            secureTextEntry={true}
            style={{borderBottomColor: '#000', borderBottomWidth: 1}}
            placeholder="Password"
          />
        </View>
        <View style={{marginBottom: 10}}>
          <Button title="Login" />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          <View>
            <Text style={{width: 50, textAlign: 'center'}}>OR</Text>
          </View>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
        </View>
      </View>
      <View style={{marginBottom: 10}}>
        <Button title="Login with Google" color="red" />
      </View>
    </ScrollView>
  );
}

export default Login;
