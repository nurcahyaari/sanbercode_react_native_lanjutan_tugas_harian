import React from 'react';
import {View, Text} from 'react-native';

export const Intro = () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text>Hallo kelas React Native Lanjutan Sanbercode!</Text>
  </View>
);
