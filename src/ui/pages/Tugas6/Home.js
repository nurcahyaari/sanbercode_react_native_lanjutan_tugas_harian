/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {View, Text, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {RootContext} from '.';

// const profileImg = require('../assets/img/profile.jpg');

export const Home = () => {
  const state = useContext(RootContext);
  console.log(state);
  useEffect(() => {
    fetch('https://mainbersama.demosanbercode.com/api/venues', {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${state.token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => console.log(data))
      .catch((err) => console.log(err));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={{flex: 1}}>
      <View
        style={{
          backgroundColor: '#58d4ed',
          height: 280,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{margin: 10}}>
          {/* <Image
                // source={profileImg}
                style={{
                  width: 70,
                  height: 70,
                  borderRadius: 64,
                }}
              /> */}
        </View>
        <Text style={{color: 'white', fontWeight: 'bold', fontSize: 16}}>
          Ari Nurcahya
        </Text>
      </View>
      <View
        style={{
          zIndex: 999,
          position: 'absolute',
          top: 230,
          // flex: 1,
          width: '100%',
          // width: '80%',
        }}>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <View
            style={{
              borderRadius: 10,
              flex: 1,
              width: '90%',
              height: 280,
              backgroundColor: 'white',
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5,
            }}>
            <View
              style={{
                marginTop: 21,
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>Tanggal Lahir</Text>
              <Text>01 May 1998</Text>
            </View>
            <View
              style={{
                marginTop: 21,
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>Jenis Kelamin</Text>
              <Text>Laki-Laki</Text>
            </View>
            <View
              style={{
                marginTop: 21,
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>Hobi</Text>
              <Text>Sleeping, Thinking</Text>
            </View>
            <View
              style={{
                marginTop: 21,
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>No. Telp</Text>
              <Text>085774019998</Text>
            </View>
            <View
              style={{
                marginTop: 21,
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text>Email</Text>
              <Text>nurcahyaari@gmail.com</Text>
            </View>
            <TouchableOpacity
              onPress={state.removeToken}
              style={{
                marginTop: 21,
                backgroundColor: '#3EC6FF',
                marginHorizontal: 21,
                flexDirection: 'row',
                justifyContent: 'center',
                padding: 10,
                borderRadius: 10,
              }}>
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};
