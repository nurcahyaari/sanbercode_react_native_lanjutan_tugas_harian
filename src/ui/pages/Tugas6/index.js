import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {Home} from './Home';
import {Register} from './register';
import Login from './Login';

export const RootContext = React.createContext();

const Stack = createStackNavigator();

function AppNavigation() {
  const [isLogin, setIsLogin] = React.useState(false);
  const [token, setToken] = React.useState('');

  const onLogin = () => setIsLogin(true);
  const handleToken = (val) => {
    setToken(val);
  };
  const removeToken = () => {
    setToken('');
    setIsLogin(false);
  };

  return (
    <RootContext.Provider
      value={{
        isLogin,
        token,
        removeToken,
      }}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="login">
          <Stack.Screen
            name="Home"
            component={Home}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="register"
            component={Register}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </RootContext.Provider>
  );
}

export default AppNavigation;
