import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Button,
  TextInput,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

import TouchID from 'react-native-touch-id';

import {logoIcon} from '../../../assets';
import {useNavigation} from '@react-navigation/native';

const touchIDConfig = {
  title: 'Authenticated Required',
  imageColor: '#191919',
  imageErrorColor: 'red',
  sensorDescription: 'Touch Sensor',
  sensorErrorDescription: 'Failed',
  cancelText: 'Cancel',
};

function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();

  useEffect(() => {
    GoogleSignin.configure({
      webClientId:
        '212518144410-e7ru77i2t85l1sktaq834okkc0s3ojcv.apps.googleusercontent.com', // client ID of type WEB for your server(needed to verify user ID and offline access)
      offlineAccess: false, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      accountName: '', // [Android] specifies an account name on the device that should be used
    });
  }, []);

  const signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const info = await GoogleSignin.signIn();
      console.warn({userInfo: info});
      console.log(info);
      // setUserInfo(info);
    } catch (error) {
      console.log(error.message);
      Alert.alert(error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  const signInWithFingerPrint = () => {
    TouchID.authenticate('', touchIDConfig)
      .then((success) => {
        console.log(success);
        props.onLogin();
        props.handleToken('');
      })
      .catch((error) => Alert.alert('Authentication Failed'));
  };

  return (
    <ScrollView style={{padding: 21, backgroundColor: '#fff'}}>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={logoIcon} style={{flex: 1}} />
      </View>
      <View style={{flex: 1}}>
        <View style={{marginBottom: 10}}>
          <Text>Email</Text>
          <TextInput
            style={{borderBottomColor: '#000', borderBottomWidth: 1}}
            placeholder="Email"
            onChangeText={(text) => setEmail(text)}
          />
        </View>
        <View style={{marginBottom: 10}}>
          <Text>Password</Text>
          <TextInput
            secureTextEntry={true}
            style={{borderBottomColor: '#000', borderBottomWidth: 1}}
            placeholder="Password"
            onChangeText={(text) => setPassword(text)}
          />
        </View>
        <View style={{marginBottom: 10}}>
          <Button
            title="Login"
            onPress={async () => {
              const myHeaders = new Headers();
              myHeaders.append('Content-Type', 'application/json');

              const raw = JSON.stringify({
                email,
                password,
              });

              const requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow',
              };

              fetch(
                'https://mainbersama.demosanbercode.com/api/login',
                requestOptions,
              )
                .then((response) => response.json())
                .then((result) => {
                  if (result.success && result.token) {
                    props.onLogin();
                    props.handleToken(result.token);
                  }
                })
                .catch((error) => console.log('error', error));
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 10,
          }}>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
          <View>
            <Text style={{width: 50, textAlign: 'center'}}>OR</Text>
          </View>
          <View style={{flex: 1, height: 1, backgroundColor: 'black'}} />
        </View>
      </View>
      <View style={{marginBottom: 10}}>
        <Button title="Login with Google" color="red" onPress={signIn} />
      </View>
      <View style={{marginTop: 10}}>
        <Button
          title="Login with Fingerprint"
          color="red"
          onPress={signInWithFingerPrint}
        />
      </View>
      <View
        style={{marginTop: 5, flexDirection: 'row', justifyContent: 'center'}}>
        <TouchableOpacity
          style={{padding: 10}}
          onPress={() => navigation.navigate('register')}>
          <Text style={{color: 'blue'}}>Belum punya akun ? Daftar</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

export default Login;
