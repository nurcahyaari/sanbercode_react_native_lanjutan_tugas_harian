import React, {useState, useCallback} from 'react';
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/AntDesign';
import moment from 'moment';

export const Todo = () => {
  const [todo, setTodo] = useState([]);

  const [todoValue, setTodoValue] = useState('');

  const handleAddTodo = () => {
    console.log(todoValue);
    todo.push({
      id: moment().valueOf(),
      name: todoValue,
      date: moment().format('D/M/YYY'),
    });
    setTodo(todo);
    setTodoValue('');
  };
  const handleDeleteTodo = useCallback(
    (id) => (e) => {
      setTodo((todo) => todo.filter((item) => item.id !== id));
    },
    [],
  );
  const handleChangeTodoValue = (text) => setTodoValue(text);

  const renderTodoList = () =>
    todo.map((item) => (
      <View
        key={item.id}
        style={{
          flex: 1,
          marginTop: 10,
          backgroundColor: 'grey',
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            margin: 4,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'column'}}>
            <Text style={{margin: 4}}>{item.date}</Text>
            <Text style={{margin: 4}}>{item.name}</Text>
          </View>
          <View style={{justifyContent: 'center'}}>
            <TouchableOpacity
              onPress={handleDeleteTodo(item.id)}
              style={{
                flex: 1,
                marginLeft: 21,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon name="delete" size={30} color="#000" />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    ));

  return (
    <ScrollView style={{padding: 21}}>
      <View>
        <Text>Masukan Todolist</Text>
      </View>
      <View style={{flexDirection: 'row', flex: 1}}>
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            width: '80%',
          }}
          onChangeText={handleChangeTodoValue}
          value={todoValue}
        />
        <View style={{width: '20%'}}>
          <TouchableOpacity
            onPress={handleAddTodo}
            style={{
              backgroundColor: '#58d4ed',
              flex: 1,
              marginLeft: 21,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Icon name="plus" size={30} color="#000" />
          </TouchableOpacity>
        </View>
      </View>
      <View>{renderTodoList()}</View>
    </ScrollView>
  );
};
