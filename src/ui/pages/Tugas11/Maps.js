import React, {useEffect} from 'react';
import {View, Text} from 'react-native';
import MapboxGL from '@react-native-mapbox-gl/maps';

const TokenMapBox =
  'pk.eyJ1Ijoib2ZmaWNpYWxsaWdodGFybW91ciIsImEiOiJja2RmYmFtbmQyNm9tMnlwbWpoazJoOXdmIn0.FxPZxGPxdNV5MaZpPuXSnw';

MapboxGL.setAccessToken(TokenMapBox);
const coordinates = [
  [107.598827, -6.896191],
  [107.596198, -6.899688],
  [107.618767, -6.902226],
  [107.621095, -6.89869],
  [107.615698, -6.896741],
  [107.613544, -6.897713],
  [107.613697, -6.893795],
  [107.610714, -6.891356],
  [107.605468, -6.893124],
  [107.60918, -6.898013],
  ,
];

export const Maps = () => {
  useEffect(() => {
    const getUserLocation = async () => {
      try {
        const permission = MapboxGL.requestAndroidLocationPermissions();
      } catch (err) {
        console.log(err);
      }
    };
    getUserLocation();
  }, []);
  return (
    <View style={{flex: 1}}>
      <MapboxGL.MapView style={{flex: 1}}>
        <MapboxGL.UserLocation visible={true} />
        <MapboxGL.Camera followUserLocation={true} />
        {coordinates.map((item, index) => {
          <MapboxGL.PointAnnotation
            id={`point-${item}-${index}`}
            coordinate={item}>
            <MapboxGL.Callout
              title={`Longitude: ${item[0]} \n Latitude: ${item[1]}`}
            />
          </MapboxGL.PointAnnotation>;
        })}
      </MapboxGL.MapView>
    </View>
  );
};
