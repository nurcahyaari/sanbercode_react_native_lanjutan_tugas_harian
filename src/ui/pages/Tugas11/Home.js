import React from 'react';
import {View, ScrollView, Text, Image, Dimensions} from 'react-native';
import IconIonic from 'react-native-vector-icons/Ionicons';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';

const websiteDesign = require('../../../assets/img/website-design.png');
const ux = require('../../../assets/img/ux.png');

const color = {
  blue: '#3EC6FF',
  darkblue: '#088dc4',
  white: '#ffffff',
};

export const Home = () => {
  return (
    <ScrollView
      style={{padding: 15}}
      contentContainerStyle={{paddingBottom: 50}}>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          backgroundColor: color.blue,
          borderRadius: 5,
        }}>
        <View
          style={{
            backgroundColor: color.darkblue,
            padding: 5,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
          }}>
          <Text style={{color: color.white}}>Kelas</Text>
        </View>
        <View style={{padding: 15, flexDirection: 'row'}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconIonic
              name="logo-react"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>React Native</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconIonic
              name="logo-python"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>Data Science</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconIonic
              name="logo-react"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>React JS</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconIonic
              name="logo-laravel"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>Laravel</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          backgroundColor: color.blue,
          borderRadius: 5,
          marginTop: 15,
        }}>
        <View
          style={{
            backgroundColor: color.darkblue,
            padding: 5,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
          }}>
          <Text style={{color: color.white}}>Kelas</Text>
        </View>
        <View style={{padding: 15, flexDirection: 'row'}}>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconMaterial
              name="wordpress"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>Wordpress</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image source={websiteDesign} style={{width: 64, height: 64}} />
            <Text style={{color: color.white}}>Web Design</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <IconMaterial
              name="server"
              color="rgba(255, 255, 255, .9)"
              size={64}
            />
            <Text style={{color: color.white}}>Server</Text>
          </View>
          <View style={{flex: 1, alignItems: 'center'}}>
            <Image
              source={ux}
              style={{height: 64, width: 64}}
              width={64}
              height={64}
            />
            <Text style={{color: color.white}}>UI/UX Design</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          backgroundColor: color.darkblue,
          borderRadius: 5,
          marginTop: 15,
        }}>
        <View
          style={{
            backgroundColor: color.darkblue,
            padding: 5,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
          }}>
          <Text style={{color: color.white}}>Summary</Text>
        </View>
        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>React Native</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>
        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>Data Science</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>React JS</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>Laravel</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>Wordpress</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>Web Design</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>Server</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>

        <View
          style={{
            backgroundColor: color.blue,
            padding: 5,
          }}>
          <Text style={{color: color.white}}>UI/UX Design</Text>
        </View>
        <View
          style={{
            padding: 5,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Today</Text>
            <Text style={{color: color.white}}>20</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 20,
            }}>
            <Text style={{color: color.white}}>Total</Text>
            <Text style={{color: color.white}}>200</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};
